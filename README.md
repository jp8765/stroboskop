# Spletni barvni stroboskop

1\. domača naloga pri predmetu [Osnove informacijskih sistemov](https://ucilnica.fri.uni-lj.si/course/view.php?id=54) (navodila)


## 6.1 Opis naloge in navodila

Na Bitbucket je na voljo javni repozitorij [https://bitbucket.org/asistent/stroboskop](https://bitbucket.org/asistent/stroboskop), ki vsebuje nedelujoč spletni stroboskop. V okviru domače naloge ustvarite kopijo repozitorija ter popravite in dopolnite obstoječo implementacijo spletne strani tako, da bo končna aplikacija z vsemi delujočimi funkcionalnostimi izgledala kot na spodnji sliki. Natančna navodila si preberite na [spletni učilnici](https://ucilnica.fri.uni-lj.si/mod/workshop/view.php?id=19181).

![Stroboskop](stroboskop.gif)

## 6.2 Vzpostavitev repozitorija

Naloga 6.2.2:

```
((UKAZI GIT)):
    - git clone https://jp8765@bitbucket.org/jp8765/stroboskop.git
    - cd stroboskop             [se prestavim v mapo '/stroboskop']
```

Naloga 6.2.3:
((UVELJAVITEV)): https://bitbucket.org/jp8765/stroboskop/commits/8495eb6ea58342b6e927e302db96116570dbf12b

## 6.3 Skladnja strani in stilska preobrazba

Naloga 6.3.1:
((UVELJAVITEV)): https://bitbucket.org/jp8765/stroboskop/commits/bd80e8db50d285e49b9dbcefa729dc525128547b

Naloga 6.3.2:
((UVELJAVITEV)): https://bitbucket.org/jp8765/stroboskop/commits/0fa14f83157900ed9a01aaeed8805da403b597cb

Naloga 6.3.3:
((UVELJAVITEV)): https://bitbucket.org/jp8765/stroboskop/commits/234af59be50c5c90dd906617631a0a0fe413ff8f

Naloga 6.3.4:
((UVELJAVITEV)): https://bitbucket.org/jp8765/stroboskop/commits/d3611666bc464abab86afbc66adc85b8f68b4a77

Naloga 6.3.5:

```
((UKAZI GIT)):
    - git checkout master
    - git merge izgled 
    - git push origin master
```

## 6.4 Dinamika in animacija na strani

Naloga 6.4.1:
((UVELJAVITEV)): https://bitbucket.org/jp8765/stroboskop/commits/24cb598aea7bcb6f7a35df5ceb43dd44fe63a977

Naloga 6.4.2:
((UVELJAVITEV)): https://bitbucket.org/jp8765/stroboskop/commits/510cb1957118d9d4cbec4232d0f323c773a71a7c

Naloga 6.4.3:
((UVELJAVITEV)): https://bitbucket.org/jp8765/stroboskop/commits/dd55adac6500cc1aab3664e723f8c21296ee4551
                 https://bitbucket.org/jp8765/stroboskop/commits/b76928b21b3b1ef3267223e5ee9c54537e3bace5       
                 !! kasneje popravljeno, zato je ločen commit. Ta popravek je tudi narejen na veji dinamika !! 
                 

Naloga 6.4.4:
((UVELJAVITEV)): https://bitbucket.org/jp8765/stroboskop/commits/261454bdb0b274d5b7a5590aacbaf368319aa27d